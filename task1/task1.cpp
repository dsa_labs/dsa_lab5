﻿#include <iostream>
#include "linked_list.h"
#include <random>
#include <vector>
#include <chrono>
#include <fstream>

#define GETTIME std::chrono::high_resolution_clock::now()
#define DURATION(a) std::chrono::duration_cast<std::chrono::microseconds>(a).count()

void selectionSort(linkedList* list) {
	auto start = GETTIME;
	node* current = list->head; // Встановлюємо покажчик current на початок зв'язного списку.
	node* min = NULL; // Створюємо покажчик min, який буде використовуватися для пошуку мінімального значення в списку.
	while (current->next) { // Поки не досягнуто кінця списку, виконуємо наступні дії
		min = current; // Копіюємо поточний вузол у min.
		node* temp = current->next; // Встановлюємо покажчик temp на наступний вузол після поточного.
		while (temp) { // Поки не досягнуто кінця списку, виконуємо наступні дії
			if (*(min->value) > *(temp->value)) { // Якщо значення вузла min більше, ніж значення вузла temp:
				min = temp; // Копіюємо вузол temp у min.
			}
			temp = temp->next; // Переходимо до наступного вузла у списку.
		}
		if (min != current) { // Якщо вузол min не є поточним вузлом:
			std::swap(min->value, current->value); // Міняємо місцями значення вузлів min та current.
		}
		current = current->next; // Переходимо до наступного вузла у списку.
	}
	auto end = GETTIME;
	std::ofstream file("output.txt", std::ios::app);
	file << list->size << std::endl;
	file << std::endl;
	file << "Time taken: " << DURATION(end - start) << std::endl;
	file.close();
}

void insertionSort(linkedList* list) {
	auto start = GETTIME;
	node* current = list->head; // Встановлюємо покажчик current на початок зв'язного списку.
	node* temp = NULL; // Створюємо тимчасовий покажчик temp, який буде використовуватися для переміщення по списку.
	while (current) { // Поки не досягнуто кінця списку, виконуємо наступні дії
		temp = current; // Копіюємо поточний вузол у temp.
		while (temp->prev && *(temp->value) < *(temp->prev->value)) { // Поки поточний вузол temp не знаходиться на початку списку і його значення менше, ніж значення попереднього вузла:
			std::swap(temp->value, temp->prev->value); // Міняємо місцями значення поточного та попереднього вузлів.
			temp = temp->prev; // Переміщуємо temp на попередній вузол.
		}
		current = current->next; // Переходимо до наступного вузла у списку.
	}
	auto end = GETTIME;
	std::ofstream file("output.txt", std::ios::app);
	file << list->size << std::endl;
	file << std::endl;
	file << "Time taken: " << DURATION(end - start) << std::endl;
	file.close();
}

void insertionSort(std::vector<int>* list) {
	auto start = GETTIME;
	for (int i = 1; i < list->size(); i++) { // Починаємо з другого елементу масиву.
		int key = list->at(i); // Зберігаємо значення поточного елементу у змінній key.
		int j = i - 1; // Встановлюємо змінну j на попередній елемент до поточного.
		while (j >= 0 && list->at(j) > key) { // Поки j не вийшов за межі масиву та значення елементу j більше, ніж key:
			list->at(j + 1) = list->at(j); // Переміщуємо елемент j на одну позицію вперед.
			j--; // Зменшуємо j на одиницю.
		}
		list->at(j + 1) = key; // Вставляємо key на відповідну позицію.
	}
	auto end = GETTIME;
	std::ofstream file("output.txt", std::ios::app);
	file << list->size() << std::endl;
	file << std::endl;
	file << "Time taken: " << DURATION(end - start) << std::endl;
	file.close();

}
 
int main()
{
	linkedList* list = initList();
	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_int_distribution<> dis(-32767, 32767);
	std::vector<int> vec;
	for (int i = 0; i < 10000; i++) {
		list->pushBack(std::make_shared<int>(dis(gen)));
		vec.push_back(dis(gen));
	}
	insertionSort(&vec);
	deleteList(list);
	return 0;

}

