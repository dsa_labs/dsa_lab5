#include "linked_list.h"
#include<stddef.h>
#include <corecrt_malloc.h>
#include <stdio.h>

linkedList* initList()
{
	linkedList* list = new linkedList();
	if (list) {
		list->head = list->tail = NULL;
	}
	return list;
}

void deleteList(linkedList* list)
{
	node* head = list->head;
	node* next = NULL;
	while (head) {
		next = head->next;
		free(head);
		head = next;
	}
	free(list);
	list = NULL;
}

bool linkedList::isEmpty()
{
	return (head == nullptr && tail == nullptr);
}

void linkedList::pushFront(const type& data)
{
	node* newNode = new node();
	newNode->value = data;
	newNode->next = head;
	newNode->prev = NULL;

	if (!isEmpty())
		head->prev = newNode;
	else
		tail = newNode;
	head = newNode;
	size++;
}

void linkedList::popFront()
{
	node* _node;
	_node = head;
	head = head->next;
	if (!isEmpty())
		head->prev = nullptr;
	else
		tail = NULL;
	size--;
	free(_node);
}

void linkedList::pushBack(const type& data)
{
	node* newNode = new node();
	newNode->value = data;
	newNode->next = NULL;
	newNode->prev = tail;

	if (!isEmpty())
		tail->next = newNode;
	else
		head = newNode;
	tail = newNode;
	size++;


}

void linkedList::popBack()
{
	node* _node = NULL;
	_node = tail;
	tail = tail->prev;
	if (!isEmpty())
		tail->next = NULL;
	else
		head = NULL;
	size--;
	free(_node);
}

node* linkedList::getNode(int index)
{
	node* _node = NULL;
	int i;
	if (index < size / 2)
	{
		i = 0;
		_node = head;
		while (_node && i < index) {
			_node = _node->next;
			i++;

		}
	}
	else {
		i = size - 1;
		_node = tail;
		while (_node && i > index) {
			_node = _node->prev;
			i--;
		}
	}
	return _node;
}

void linkedList::printList()
{
	node* _node = head;
	while (_node) {
		printf("%d\n", *(_node->value));
		_node = _node->next;
	}
}

