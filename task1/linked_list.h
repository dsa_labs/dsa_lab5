#include <iostream>


typedef std::shared_ptr<int> type;

struct node {
	type value;
	struct node* prev;
	struct node* next;
};

struct linkedList {
	struct node* tail;
	struct node* head;
	size_t size;

	bool isEmpty();
	void pushFront(const type& data);
	void popFront();
	void pushBack(const type& data);
	void popBack();
	node* getNode(int index);
	void printList();

};

linkedList* initList();
void deleteList(linkedList* list);




